#!/bin/sh
# This script moves a markdown file into the posts folder.
# It adds frontmatter with the date. Assumes no frontmatter exists.

# Expect a single argument.
if [ $# -ne 1 ]; then
    echo "Usage: post.sh <filename>"
    exit 1
fi

DATEFILE=$(date +%Y-%m-%d)
DATEUNIX=$(date +%s)
DATEFRONTMATTER=$(date '+%b %d, %Y')

OUTFILE=src/routes/twits/posts/"$DATEFILE-$DATEUNIX.md"
if [ -f "$OUTFILE" ]; then
    echo "Oh no somehow $OUTFILE already exists."
    exit 1
fi

# Prepend frontmatter with date.
echo '---
date: '"$DATEFRONTMATTER"'
dateunix: '"$DATEUNIX"'
---
' | cat - "$1" > "$OUTFILE"

echo "" > "$1"

echo "Created $OUTFILE"

git add "$OUTFILE"

echo "Added $OUTFILE to git"

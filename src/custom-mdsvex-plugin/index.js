import { visit } from 'unist-util-visit';

function customPlugin() {
  return (tree) => {
    visit(tree, 'paragraph', (node) => {
      // Create Youtube embeds.
      // mdsvex converts urls into hyperlinks.
      if (
        node.children.length === 1 &&
        node.children[0].type === 'link' &&
        node.children[0].children.length === 0
      ) {
        const regex = /https:\/\/www\.youtube\.com\/watch\?v=(.+)/;
        const match = node.children[0].url.match(regex);
        if (match) {
          node.children = [
            {
              type: 'text',
              value: `<iframe width="560" height="315" src="https://www.yewtu.be/embed/${match[1]}?autoplay=0"></iframe>`
            }
          ];
        }
      }
    });

    // This is a tweaked version of remark-code-titles
    visit(tree, 'code', (node, index) => {
      const nodeLang = node.lang || '';
      let language = '',
        title = '',
        href = '',
        hyperlink_pre = '',
        hyperlink_post = '';

      if (nodeLang.includes(':')) {
        language = nodeLang.slice(0, nodeLang.indexOf(':'));
        title = nodeLang.slice(nodeLang.indexOf(':') + 1, nodeLang.length);
      }

      if (!title) {
        return;
      }
      title = title.trim();

      // Detect and parse markdown hyperlinks. Only supports links in isolation.
      if (title.startsWith('[')) {
        href = title.substr(title.indexOf('(') + 1);
        // Drop the final ')'
        href = href.substr(0, href.length - 1);

        title = title.substr(1, title.indexOf(']') - 1);
      }

      // Just going to dump the style in here to not have to import any extra
      // css files.
      const style =
        'margin-bottom: -11px; filter: brightness(120%); border-bottom-left-radius: 0px; border-bottom-right-radius: 0px;';
      if (href) {
        hyperlink_pre = `<a class ="token variable" href="${href}">`;
        hyperlink_post = '</a>';
      }
      // Use <pre> and <code> to inherit our prism styles.
      const titleNode = {
        type: 'html',
        value: `<pre style="${style}" class="language-undefined"><code class="language-undefined">${hyperlink_pre}${title}${hyperlink_post}</code></pre>`
      };

      tree.children.splice(index, 0, titleNode);
      node.lang = language;
    });
  };
}

export default customPlugin;

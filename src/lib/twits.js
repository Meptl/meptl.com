import prisma from '$lib/prisma';

export const fetchTwits = async () => {
  const allPostFolders = import.meta.glob('/src/routes/twits/posts/*.md');
  const iterablePostFolders = Object.entries(allPostFolders);

  const allPosts = await Promise.all(
    iterablePostFolders.map(async ([_path, resolver]) => {
      const resolved = await resolver();
      const content = resolved.default.render().html;

      return {
        meta: resolved.metadata,
        likes: 0,
        retweets: 0,
        content: content
      };
    })
  );
  return allPosts;
};

export const fetchSortedTwits = async () => {
  const allPosts = await fetchTwits();

  const sortedPosts = allPosts.sort((a, b) => {
    return b.meta.dateunix - a.meta.dateunix;
  });

  return sortedPosts;
};

export const fetchSortedTwitsLiked = async () => {
  let allPosts = await fetchSortedTwits();

  try {
    const ret = await prisma.twit.findMany({
      where: {
        id: { in: allPosts.map((post) => post.meta.dateunix) }
      }
    });

    const dict = allPosts.reduce((acc, item) => {
      acc[item.meta.dateunix] = item;
      return acc;
    }, {});

    for (const post of ret) {
      dict[parseInt(post.id)]['likes'] = post.likes;
      dict[parseInt(post.id)]['retweets'] = post.retweets;
    }
  } catch (e) {
    console.log(e);
  }
  return allPosts;
};

export const fetchMarkdownPosts = async () => {
  const allPostFolders = import.meta.glob('/src/routes/blog/**/+page.md');
  const iterablePostFolders = Object.entries(allPostFolders);

  const allPosts = await Promise.all(
    iterablePostFolders.map(async ([path, resolver]) => {
      const { metadata } = await resolver();
      const postPath = path.slice(11, -9);

      return {
        meta: metadata,
        path: postPath
      };
    })
  );

  return allPosts;
};

export const fetchSortedMarkdownPosts = async () => {
  const allPosts = await fetchMarkdownPosts();

  const sortedPosts = allPosts.sort((a, b) => {
    const d1 = new Date(a.meta.date);
    const d2 = new Date(b.meta.date);
    return d2 - d1;
  });
  return sortedPosts;
};

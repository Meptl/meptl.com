import { writable } from 'svelte/store';

export const spectrum = writable({
  bass: 0,
  mids: 0,
  highs: 0
});

let audioContext;
let analyser;
// Map HTML elements to boolean "playing"
let audioConnections = new Map();

export function fixIOSAudioContext() {
  if (audioContext) {
    return;
  }

  setupAudioGrapher();

  // For iOS the AudioContext needs to be "warmed up". Play an empty sound.
  // Also this must be called _after_ a user event (touch event).
  var buffer = audioContext.createBuffer(1, 1, 22050);
  var source = audioContext.createBufferSource();
  source.buffer = buffer;
  source.connect(audioContext.destination);
  if (source.start) {
    source.start(0);
  } else if (source.play) {
    source.play(0);
  } else if (source.noteOn) {
    source.noteOn(0);
  }
}

// Attach this function to the "play" event of audio elements.
export function onPlayAudioGrapher(ev) {
  if (!audioContext) {
    setupAudioGrapher();
  }

  audioContext.resume();

  if (!audioConnections.has(ev.srcElement)) {
    let source = audioContext.createMediaElementSource(ev.srcElement);
    source.connect(analyser);
  }

  audioConnections.set(ev.srcElement, true);
}

// Call this function on "pause" events
export function onPauseAudioGrapher(ev) {
  audioConnections.set(ev.srcElement, false);
  if (!Array.from(audioConnections.values()).includes(true)) {
    // Save some CPU if we aren't playing anything.
    audioContext.suspend();
  }
}

export function clearConnections() {
  if (!audioContext) {
    return;
  }

  audioContext.suspend();
  audioConnections.clear();
}

function splitFrequencyBins(dataArray) {
  // indices must be below dataArray bufferLength which is half of
  // analyser.fftSize.
  const bassStart = 0;
  const bassEnd = bassStart + 6;
  const midsStart = bassEnd + 1;
  const midsEnd = midsStart + 12;
  const highsStart = midsEnd + 1;
  const highsEnd = highsStart + 24;

  let bassSum = 0;
  let midsSum = 0;
  let highsSum = 0;
  for (let i = bassStart; i <= bassEnd; i++) {
    bassSum += dataArray[i];
  }
  for (let i = midsStart; i <= midsEnd; i++) {
    midsSum += dataArray[i];
  }
  for (let i = highsStart; i <= highsEnd; i++) {
    highsSum += dataArray[i];
  }

  const bassAvg = bassSum / (bassEnd - bassStart + 1);
  const midsAvg = midsSum / (midsEnd - midsStart + 1);
  const highsAvg = highsSum / (highsEnd - highsStart + 1);

  return {
    bass: bassAvg,
    mids: midsAvg,
    highs: highsAvg
  };
}

function setupAudioGrapher() {
  audioContext = new (window.AudioContext || window.webkitAudioContext)();
  analyser = audioContext.createAnalyser();
  analyser.connect(audioContext.destination);

  analyser.fftSize = 2048;
  const bufferLength = analyser.frequencyBinCount;
  const dataArray = new Uint8Array(bufferLength);

  function draw() {
    analyser.getByteFrequencyData(dataArray);

    let { bass, mids, highs } = splitFrequencyBins(dataArray);
    // Values range from 0-255 by default
    bass = Math.max(bass - 97.0, 0) / 158.0;
    mids = Math.max(mids - 97.0, 0) / 158.0;
    highs = Math.max(highs - 97.0, 0) / 158.0;

    spectrum.set({
      bass: bass,
      mids: mids,
      highs: highs
    });

    requestAnimationFrame(draw);
  }
  draw();
}

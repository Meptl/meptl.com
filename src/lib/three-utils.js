import * as THREE from 'three';
import { STLLoader } from 'three/examples/jsm/loaders/STLLoader.js';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';

export function defaultScene(el) {
  const scene = new THREE.Scene();

  // Add lightning. Ambient, Sky/Sun, and Directional.
  let light = new THREE.AmbientLight(0xffffff, 0.4);
  scene.add(light);
  light = new THREE.HemisphereLight(0xffffff, 0x000000, 1.8);
  scene.add(light);
  light = new THREE.DirectionalLight(0xffffff, 1.7);
  light.position.set(10, 10, 10);
  light.target.position.set(0, 0, 0);
  scene.add(light);
  light = new THREE.DirectionalLight(0xffffff, 1);
  light.position.set(-10, -10, -10);
  light.target.position.set(0, 0, 0);
  scene.add(light);

  return scene;
}

// Returns the loaded mesh.
export async function loadSTL(scene, path, scale = 1.0, rotX = 0, rotY = 0, rotZ = 0) {
  const loader = new STLLoader();
  return loader.loadAsync(path).then((geometry) => {
    const material = new THREE.MeshPhongMaterial({ color: 0xffbb3e });
    const mesh = new THREE.Mesh(geometry, material);
    mesh.position.set(0, 0, 0);
    mesh.scale.set(scale, scale, scale);
    mesh.rotation.set(
      THREE.MathUtils.degToRad(rotX),
      THREE.MathUtils.degToRad(rotY),
      THREE.MathUtils.degToRad(rotZ)
    );
    mesh.castShadow = true;
    mesh.receiveShadow = true;

    geometry.center();
    scene.add(mesh);
    return mesh;
  });
}

// Only supports a square container/canvas. Sets up Orbit controls.
export function setupRenderer(container, canvasEl, scene, camera, canPan = false) {
  let renderer;
  function animate() {
    requestAnimationFrame(animate);
    renderer.render(scene, camera);
  }

  renderer = new THREE.WebGLRenderer({ antialias: true, canvas: canvasEl, alpha: true });
  renderer.setClearColor(0x000000, 0.1);

  // Create camera controls.
  let controls = new OrbitControls(camera, renderer.domElement);
  controls.enableZoom = true;
  controls.enableRotate = true;
  controls.enablePan = canPan;
  controls.saveState();
  controls.reset();

  // We're adding this to the controls for convenience.
  controls.resize_handler = (newWidth) => {
    // Force square aspect ratio. Because the parent div has to "contain" this
    // canvas, it doesn't maintain its square ratio when resizing the page.
    renderer.setSize(container.clientWidth, container.clientWidth);
    camera.aspect = 1;
    camera.updateProjectionMatrix();
  };

  controls.resize_handler();
  animate();
  window.addEventListener('resize', controls.resize_handler);

  return [controls, renderer];
}

export function updateRenderer(container, renderer, camera) {
  renderer.setSize(container.clientWidth, container.clientHeight);
  camera.aspect = container.clientWidth / container.clientHeight;
  camera.updateProjectionMatrix();
}

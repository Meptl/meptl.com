export const prerender = true;

import { browser } from '$app/environment';
import { fixIOSAudioContext, clearConnections } from '$lib/soundstore.js';

let audioContextFixed = false;

export const load = ({ url }) => {
  const currentRoute = url.pathname;

  if (browser) {
    if (!audioContextFixed) {
      document.addEventListener('touchend', fixIOSAudioContext);
      audioContextFixed = true;
    }
    clearConnections();
  }

  return {
    currentRoute
  };
};

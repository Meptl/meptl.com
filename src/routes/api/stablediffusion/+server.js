export const prerender = false;

import { json } from '@sveltejs/kit';
import { RUNPOD_API_KEY } from '$env/static/private';
import runpod_request from './runpod_request.json';

const endpoint = 'sf5hts1qs4xipf';
export const POST = async ({ request }) => {
  const { prompt } = await request.json();
  // I hope multithreaded-ness doesn't muck with this :)
  runpod_request['input']['workflow']['6']['inputs']['text'] = prompt;
  const response = await fetch(`https://api.runpod.ai/v2/${endpoint}/run`, {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${RUNPOD_API_KEY}`,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(runpod_request)
  });
  const data = await response.json();
  const id = data.id;
  console.log(`Created job ${id}`);

  await new Promise((resolve) => setTimeout(resolve, 2000));

  // Poll for job status with exponential backoff.
  let delay = 200;
  const maxDelay = 60000;
  while (true) {
    if (delay > maxDelay) {
      console.log('Job timed out');
      return json({ error: 'Job timed out' }, { status: 504 });
    }
    await new Promise((resolve) => setTimeout(resolve, delay));

    const statusResponse = await fetch(`https://api.runpod.ai/v2/${endpoint}/status/${id}`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${RUNPOD_API_KEY}`
      }
    });
    const statusData = await statusResponse.json();
    const status = statusData.status;

    if (status === 'COMPLETED') {
      console.log(`Job completed: ${id}`);
      return json({ data: statusData['output']['message'] }, { status: 200 });
    } else if (status === 'IN_PROGRESS' || status === 'IN_QUEUE') {
      console.log('Job in progress...');
      delay *= 2;
    } else {
      return json({ error: `Job failed with status: ${status}` }, { status: 400 });
    }
  }
};

import { json } from '@sveltejs/kit';
import { fetchTwits } from '$lib/twits.js';
import prisma from '$lib/prisma';

export const POST = async (req) => {
  const allPosts = await fetchTwits();
  if (!allPosts.some((p) => p.meta.dateunix == req.params.id)) {
    return json({}, { status: 404 });
  }
  // Find the twit with the specified dateunix ID
  try {
    await prisma.twit.upsert({
      where: {
        id: parseInt(req.params.id)
      },
      create: {
        id: parseInt(req.params.id),
        likes: 1,
        retweets: 0
      },
      update: {
        likes: {
          increment: 1
        }
      }
    });
    return json({}, { status: 200 });
  } catch (e) {
    console.log(e);
    return json({ error: 'Internal Server Error' }, { status: 500 });
  }
};

import { json } from '@sveltejs/kit';
import { fetchSortedMarkdownPosts } from '$lib/blogposts.js';

export const load = async () => {
  let allPosts = await fetchSortedMarkdownPosts();
  return { posts: await json(allPosts).json() };
};

---
title: Cornbread Recipe
date: Nov 5, 2023
categories:
  - cooking
---

This is a ratio recipe.
Everything is volumetrically relative to each other.
In short, you decide how much milk you want to add
and that one measurement determines how much of the other ingredients you'll use.
I've estimated non-measurable ingredients relative to what I think an imperial
cup is so if your unit of volume is close to 1 cup it'll be a close estimation.
Warning: I haven't used an imperial cup in a _long_ time.

- 0.25 egg (about 1 whole egg for 1 imperial cup)
- 0.5 melted butter (1 stick of butter melted for 1 imperial cup)
- 1 milk
- 0.75 sugar
- ~2\* dry mix consisting of 1:1 corn meal and flour
- \* salt (1 tsp for 1 imperial cup)
- \* baking soda (1 tsp for 1 imperial cup)

You can use your own measuring cup, here's mine:

![](./cup.webp)

But the best part is, if you have an eye for volumes, you don't need to measure
at all!
My mental model of an imperial cup is somewhere along the top of the handle of
this cup.

Salt and baking soda should be about a teaspoon.
Add dry mix until you get a thicc pancake-like consistency:

![](./cornbread-consistency.mp4)

Cook until slightly browned and when you try to eat it, it is solid.
Poking a pokey device into the product should not leave anything sticking on the
pokey device.
Maybe... 30 minutes? But bro I don't even preheat my oven.

The general recommendation is 350°F (~180°C) as that is the temperature
that sugar burns and slightly above sugar's delicious Maillard reaction.
But all ovens are different and unless you've calibrated yours
(which is a thing you can do),
you'll have to figure out what temperature is best for baking.

## Substitutions

### Egg

I've found flax (with some water) to be a decent replacement!
And probably eats less animals.

### Melted Butter

You can generally use any oil you have at hand.
To melt butter, I mix the milk and butter and microwave it;
The milk prevents the butter from popping/splattering.

### Milk

Not really a substitution,
I just want to mention if you want to have some makeshift buttermilk,
you can add a small amount of acid e.g. lemon into the milk.
Mix and let it sit for a bit.
I dunno... two minutes?

### Everything

Have fun!
Replace some of the milk with that disgusting egg-nog you got because
you've always wondered what those Christmas tunes were ranting about.
These eggs seem pretty small -- put two of them!
Replace that white granulated sugar with honey or brown sugar or the dust at the
bottom of your bag of cereal!
I highly recommend adding some cracked corn.
The world is your oyster and this cornbread is your canvas!

## Troubleshooting

**I**: It's bitter!<br>
**A**: You added too much baking soda.

**I**: It's salty!<br>
**A**: You added too much salt.

**I**: I have extra dry mix!<br>
**A**: Save it for later; Use as a flour substitute.

![](./meme.webp)

## Emotional Backstory

When I was a kid, I baked goods.
I still do, but I also did it as a kid.
My mom doesn't make baked goods.
And most times my sister was up and about going to school and having a job.
But sometimes someone that wasn't my mother would want to bake something and we
got to pass the task of mixing the cookie dough around because your arm would
get really tired after you added the flour to the recipe.

So baking today really reminds me of those precious moments where I interpreted
celsius as fahrenheit and converted my macaroons into a puddle of sugar.

## Bonus Pancake Recipe

I make my pancakes in a similar manner:

- 0.25 egg (I just use 1 whole egg)
- 1 milk
- 0.25 sugar
- \* salt
- \* baking soda
- \* flour until it looks like pancake mix

Although I imagine my egg ratio is a bit high most times because I make a single
serving of pancakes.

---
title: Creating DRM-free eBooks from ACSM files
date: 'Dec 25, 2022'
categories:
  - linux
---

When I borrowed a book from my local library's e-content distributor,
_overdrive.com_,
I was given an Adobe Content Server Message (ACSM) file.
I muddled through a few options before finding a fairly turnkey solution to
make the book usable on my Kindle Paperwhite 2 (2013).
The full model name is for future me 😁.

The ACSM file I got was a 1.8KB XML file which in short says:
"Your book is another file."
Typically, you'd use
[Adobe Digital Editions](https://en.wikipedia.org/wiki/Adobe_Digital_Editions)
to download the content,
but they don't have a Linux client.
For the neckbeards, there exists a toolset called [libgourou](https://indefero.soutade.fr/p/libgourou/)
which has reimplemented the protocol for downloading from Adobe's servers.

Firstly, you have to create an account at Adobe, the credentials which will
be given to libgourou.
I use NixOS, a Linux distribution where nothing works,
so instead of running a custom nix package for libgourou
only to find that the program simply segfaults,
I used a [docker container with libgourou](https://github.com/bcliang/docker-libgourou)

Build it yourself or play the docker lottery with:

```sh
mkdir adept
docker run \\
    -v $(pwd)/adept:/home/libgourou/.adept \\
    -v $(pwd):/home/libgourou/files \\
    --rm -it --entrypoint /bin/bash \\
    bcliang/docker-libgourou
```

Within the container, login to your Adobe account:

```sh
adept_activate -u adobe.kf9rh@meptl.com
```

The above will create a credentials file /home/libgourou/.adept.
From here you can download your book from Adobe and remove the DRM:

```sh
acsmdownloader -f files/FILE.acsm -O files
adept_remove -f files/BOOK.epub
```

The entrypoint script for the container actually does the download and DRM
removal for you.
Since the credentials are stored in a file
and we're using mounted volumes so that files persist on the host system,
subsequent runs to download an acsm file can be done with a one-liner from the
host system (as long as your login credentials haven't expired):

```sh
docker run \\
    -v $(pwd)/adept:/home/libgourou/.adept \\
    -v $(pwd):/home/libgourou/files \\
    --rm -it bcliang/docker-libgourou FILE.acsm
```

And that's it!

The downloaded file is placed in the current directory.
From here, we'll use a tool that comes with the Calibre package to convert EPUB files to MOBI files,
because my particular Kindle does not support EPUB files.

```sh
ebook-convert foo.epub bar.mobi
```

# Calibre

This post ended up being fairly anti-climatic.
Initially, I didn't realize libgourou came with a tool to remove the DRM on the downloaded EPUB,
so I used a [Calibre plugin](https://github.com/noDRM/DeDRM_tools) which didn't come with much documentation (that I could find).
I gave that plugin a key created by `acsmdownloader --export-private-key` to deal with DRM removal.

Anywho, I heard through the grapevine that there are also plugins to enable the downloading of ACSM files in Calibre directly,
so it should be possible to have the entire workflow integrated into a single program.

Personally, I don't use Calibre so having this is not of interest to me.

---
title: The Weakness of Cheap Suitcases
date: Aug 21, 2024
snippet: All I have are cheap suitcases. Not because I purchase cheap suitcases, but because I don't purchase suitcases.
categories:
  - hardware
---

<script>
    import STLCanvas from '$lib/STLCanvas.svelte';

    const containerClasses = "max-w-96";
    const zoom = 8;
</script>

All I have are cheap suitcases.
Not because I purchase cheap suitcases, but because I don't purchase suitcases.
And all these garbage suitcases fail in the same place: the wheels.
They lock up, crack, and fail every which way
and because the wheel and axle assembly is typically riveted,
there's no way of simply replacing just the part that is failing.

Fortunately or unfortunately,
the entire wheel assembly is removeable via screws in the suitcase body.
With the original wheel assembly removed and mounting hardware retrieved,
I can now fix my broken suitcases!
All it will take is a 3d-printer, the printing community's favorite bearing, superglue,
8mm aluminum rods, and other various bits and bobs.

![](./wheel2.webp)

And the best part is I can reuse the wheel assembly and only have to redesign
the piece that attaches to different suitcases!
Here's the model of all 3d printed pieces of the wheel assembly in their final configuration.

<STLCanvas stl_path='/pub/stls/cheap-suitcases/suitcase-wheel.stl' rotZ=135 {containerClasses} {zoom} />

These pieces are reused between suitcases.
The tire is printed in TPU so it has a bit of give to it.
This takes two 608RS bearings: one at the top and another in the wheel hub.
These are friction fit, but because I suck at 3d printing,
I always have to manually adjust the hole anyway.
And I dump some superglue in it for good measure.
Should I use some form of epoxy? Probably.
But I'll deal with that issue when I run into it!
A rod acts as an axle the wheel and is held in place with some locknuts.

Here's a couple mating pieces for suitcases:

<div class="flex">
    <STLCanvas stl_path='/pub/stls/cheap-suitcases/suitcase-mate2.stl' rotZ=135 rotX=-90 tilt=5 {containerClasses} {zoom} />
    <STLCanvas stl_path='/pub/stls/cheap-suitcases/suitcase-mate.stl' rotZ=135 rotX=-90 tilt=5 {containerClasses} {zoom} />
</div>

Each piece matches the screw profile and external shape of the OEM part.
I superglue a 8mm rod into the bottom which mates with a bearing on the wheel assembly.

One issue is that the OEM wheel assembly must be smaller than this wheel assembly.
Otherwise we can't adjust the height of the mating piece to compensate for the
height of the OEM setup.
Although... the problem is a bit self inflicted because I wanted a beefcake of a wheel.
For my first suitcase, I had to replace all four wheels to keep it balanced.
This is fine, the original were stinky
! <small>Copium.</small>

![](./wheel1.webp)

The final assembly.
All the 3d printed bits are in my [cad graveyard](https://gitlab.com/Meptl/cad/-/tree/main/suitcase-wheel)
in a single [Plasticity](https://www.plasticity.xyz/) file.

Anyway the wheels immediately broke when I stuffed 60lb onto the dang thing.
![](./broken-wheels.webp)

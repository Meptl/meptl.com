---
title: Dewtime
date: Aug 17, 2022
dateunix: 1660769032
---

<!-- Workaround from https://github.com/pngwn/MDsveX/issues/287 -->
<script context="module">
    import image from "./dewtime.webp"
    metadata.image = image
</script>

[dewtime.meptl.com](https://dewtime.meptl.com)

My personal task tracking website.
I don't really expect it to be something anyone _but_ me can use... but look
it's a thing!
I stopped using it because my timer watch
[Watchy](https://github.com/sqfmi/Watchy)
is broken and I refuse to deal with it.

Svelte. Svelte. Svelte. Svelte.

---
title: Virtual Luv
date: Oct 26, 2023
dateunix: 1698305540
---

<!-- Workaround from https://github.com/pngwn/MDsveX/issues/287 -->
<script context="module">
    import image from "./virtual-luv.webp"
    import audio from "./virtual-luv.mp3"

    metadata.image = image
    metadata.audio = audio
</script>

<script>
    import TextAudio from '$lib/TextAudio.svelte';
    import commentaudio from "./ayaya.mp3";
</script>

Cover of [a cover of Virtual Luv feat. tofubeats by 昼森やも with とげお](https://www.youtube.com/watch?v=rH_B2yEhs1k).

Their cover is better than the
[original by VaVa](https://www.youtube.com/watch?v=lmieiQoR7uc).
Although I'm unreasonably angry that VaVa doesn't do the final verse of the
chorus in one breath in his live performances.
Literally every performance the man taps out. Tsk tsk.

昼森やも's cover is even better than the version done
by MY ONE TRUE WEEB LOVE (I'm not a weeb though).
I've said it once and I'll say it again, the real ones cover this song.
Not me though, I didn't mix my own tune.

This is my japanese entry.
Obligatory <TextAudio audio={commentaudio}>AYAYA. </TextAudio>

---
title: Dynamo of Volition
date: Jul 22, 2023
dateunix: 1690038153
---

<!-- Workaround from https://github.com/pngwn/MDsveX/issues/287 -->
<script context="module">
    import image from "./dynamo-of-volition.webp"
    import audio from "./dynamo-of-volition.mp3"

    metadata.image = image
    metadata.audio = audio
</script>

Cover of
[Dynamo of Volition by Jason Mraz](https://www.youtube.com/watch?v=xESZUB5Mcrc).

I was a fan of this dude somewhere around
the time when "I'm Yours" was all over the radio -
so presumably 2008.
Crazy how I can remember most of the lyrics to his songs.
Even today, sometimes they pop up in my rambles.

Some other songs of his I considered in my "Ode to things I've loved":
Dreamlife of Rand McNally, Sleep All Day, Geek in the Pink.
All the lyrics to which I can tell you **RIGHT NOW**.

P.S. Figure out your plugin pipeline before orienting your track regions.

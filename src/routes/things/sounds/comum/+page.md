---
title: Comum
date: Sep 22, 2023
dateunix: 1695361977
---

<!-- Workaround from https://github.com/pngwn/MDsveX/issues/287 -->
<script context="module">
    import image from "./comum.webp"
    import audio from "./comum.mp3"

    metadata.image = image
    metadata.audio = audio
</script>

<script>
    import TextAudio from '$lib/TextAudio.svelte';
    import commentaudio from "./session.mp3";
</script>

<small>
<strong>Edit</strong>: I censored the drawing because I think I should stick to being imaginary!
I'll put some garbage anime shit into it eventually maybe.
</small>

Cover of
[Comum by ÀVUÀ](https://www.youtube.com/watch?v=xKSl4EvdqRY).

Te Encontrar is also an amazing song by them!

This is my first and portuguese entry into
"a song in every language".
Although perhaps I can stone two birds by saying
[Dynamo of Volition](/things/sounds/dynamo-of-volition)
is an English song.
In case it isn't obvious, I don't speak every language.

I did think of maybe doing a lovey duo in every language
because surely that must exist;
ÀVUÀ is clearly just Us The Duo speaking Brazil.
(Although ÀVUÀ isn't in a relationship)
But if I do lovey duos I'll just be singing a bunch of songs where
one or both voices isn't within the comfortable range for my voice.
Also I don't think I'm quite... tonally-aligned with love-speak.

<TextAudio audio={commentaudio}>
I do love me though!
</TextAudio>

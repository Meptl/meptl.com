---
title: Boondocks Theme
date: Jul 23, 2023
dateunix: 1690084980
shitpost: true
---

<!-- Workaround from https://github.com/pngwn/MDsveX/issues/287 -->
<script context="module">
    import image from "./boondocks.webp"
    import audio from "./boondocks.mp3"

    metadata.image = image
    metadata.audio = audio
</script>

Look Ma, I got a rap career!

Apparently, I don't know how to pronounce "dark".

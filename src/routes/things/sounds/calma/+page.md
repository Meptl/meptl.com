---
title: Calma
date: Nov 13, 2024
dateunix: 1731520524
---

<!-- Workaround from https://github.com/pngwn/MDsveX/issues/287 -->
<script context="module">
    import image from "./calma.webp"
    import audio from "./calma.mp3"

    metadata.image = image
    metadata.audio = audio
</script>

Cover of [Calma by Pedro Capó feat. Farruko](https://www.youtube.com/watch?v=1_zgKRBrT0Y).

This song was all the rage during the weeks around when I was hanging out in
Colombia.

I don't really like this one for some reason.
It makes me feel a particular cringe!
Also I didn't sign up to be a music producer...
I just want to make funny noises with my mouth.

This is my spanish entry.

Man this note has gone through A LOT.
![](./calma-notes.webp)

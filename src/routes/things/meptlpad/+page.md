---
title: Meptlpad
date: Oct 28, 2023
dateunix: 1698516363
---

<!-- Workaround from https://github.com/pngwn/MDsveX/issues/287 -->
<script context="module">
    import image from "./meptlpad.webp"
    metadata.image = image
</script>

<script>
    import STLCanvas from '$lib/STLCanvas.svelte'
</script>

A 4x4 keypad with print-in-place keyswitches

<STLCanvas stl_path='/pub/stls/meptlpad-topcase.stl' rotX=-90 rotZ=225 zoom=10 />

I have a blog post going over the nitty gritty [here](/blog/meptlpad).

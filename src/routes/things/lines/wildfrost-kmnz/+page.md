---
date: Jun 29, 2023
dateunix: 1688025076
---

<!-- Workaround from https://github.com/pngwn/MDsveX/issues/287 -->
<script context="module">
    import image from "./wildfrost-kmnz.mp4"

    metadata.image = image
</script>

A small animated image in the style of
[Wildfrost](https://store.steampowered.com/app/1811990/Wildfrost)
of that weeb shit I'm still currently into.

I drew this with my usual
[Krita](https://krita.org)
but animated it in
[Moho](https://moho.lostmarble.com)
which I **do not** vibe with.
It's not bad; I just don't like it.
Also the trial version doesn't allow for exporting the animation;
Maddening to discover _after_ I finished everything.
Still wondering what Mihoyo uses for their web exported mesh animations.
I might have to use a program designed for vector art to fully get the style down.

I messed up the loop by having the animation curves not match at the start and the end.
Additionally the start and end frames differ largely from the middle of the
animation so I had to have a stark "return to beginning" movement.

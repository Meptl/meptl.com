---
title: KMNZ LITA 3D Model
date: Dec 13, 2023
dateunix: 1702481730
---

<script>
    import STLCanvas from '$lib/STLCanvas.svelte';
</script>

[Source files](https://gitlab.com/Meptl/kmnz).
It's my first full body 3d character
so the topology sucks, the UV unwrapping sucks, and the rigging sucks,
but such is life!
Also I didn't realize you're suppose to "apply" the metarig to get an actual
useable rig with IK and junk.

<STLCanvas stl_path=/pub/stls/lita.stl scale=4 rotX=-90 canPan=true/>

Note that I've decimated the rendered model above to about 30% of its vertices
just to save on web space.
I had big plans for this model and saw it as a framework on which a lot of
derivative work could be made.
Think SaaS product that produces selfies with your favorite tubers🥔 using an
anime facsimile of your likeness.
Unfortunately, KMNZ is scheduled to die soon.

**_Good bye, KMNZ!_**
As your #1 biggest fan, I'll never accept this.
I don't respect your resignation
and I don't understand why you had KMNZ LIZ under an "at will" contract.

I learned a lot though and could probably make another model fairly quickly.
But in what world would I be so entranced by some other collection of 2d pixels
that I would want to do _this_ again.
Also UV unwrapping 🤮.
Actually I don't think I'm fit for the 3d pipeline; I also hate rigging.

But you know what I... think I like doing?
Animations:

![](./idle.mp4)

This was suppose to just be an image, but I got carried away once I had rigged
the model.
I wanted to make one of those idle animations from Overwatch's character
selection screen.
It's a bit shitty and stiff because, again, the rig sucks
and is partially incomplete in terms of shape keys for the face.

The background is _mostly_ an AI generated image using the line art from the
final page of the KMNZ manga!

![](./manga-page.webp)

![](./manga-page-colorized.webp)

I didn't mind the poor AI rendering and my bad mixing of the composite images
because it would be in the background, blurred by depth of field, and slightly
fixed (colorwise) by ambient occlusion.
And I'll have you know, I spent **minutes** telling the AI to generate better red
texturing for the graffiti lettering.

Going back to "framework for derivative work".
Look I 3d printed a figurine and then painted it:

![](./figurine.webp)

Yes, I got a bunch of paints and brushes just for this project.
In my defense, I also had to paint an eggshell-ish color for a hole I made in my
drywall.
Lighting in this photo courtesy of [gooselight](/things/things/gooselight)!
I'm going to be defensive about why I didn't sand this because some Twitter
fiend commented on it:

1. I hate sanding
2. This was just an experimental first-try (at 0.2mm layer height)
3. There's an absolutely hideous texturing at the back caused by some fixes I did with a
   soldering iron

While I'm here, don't use spackle to fill cracks between two joined 3d-printed pieces.

## Next Steps (which likely won't be done)

I have to update to Blender 4.0 which released during the creation of this.
This includes a new Rigify armature which I should actually apply this time.

[The shader I used for the
model](https://github.com/festivities/Blender-miHoYo-Shaders)
also had an update about a week before I finished 😞.
Actually, it would be beneficial to write down how
to use that shader on a custom model because as it progresses its workflows
become more and more tied to the automated importer which only works with
datamined Genshin/HSR models.

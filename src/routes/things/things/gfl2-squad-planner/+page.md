---
title: GFL2 Squad Planner
date: Jan 14, 2025
dateunix: 1736836813
---

<script>
    import image from "./image.webp"
    metadata.image = image
</script>

[gfl2-squad-planner.meptl.com](https://gfl2-squad-planner.meptl.com)

Some weeb-shit planning app.
The squad selection in game doesn't
save the squad before actually using your crew.
Also I wanted to play with Svelte <span class="text-2xl
font-extrabold">5</span>.

I do find the UI in the game quite pretty.
Drop-shadow on steel gray texture with low-radius corners.
Flat highlight colors and then they suplexed me with a dotted divider
reminiscing a carnival ticket.
The actual navigation tree in game however is absolute garbage.
Literally the stupidest layout of items I've ever seen,
though I suppose that's par for
the course in <span class="lugrasimo">gamba</span> games.

<style>
  .lugrasimo {
    font-family: Lugrasimo
  }
</style>

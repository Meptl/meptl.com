---
title: gooselight
date: Dec 03, 2023
dateunix: 1701535172
---

<!-- Workaround from https://github.com/pngwn/MDsveX/issues/287 -->
<script context="module">
    import image from "./gooselight.webp"
    metadata.image = image
</script>

An adjustable and clampable LED light.
Build instructions in
[some folder in my cad dumping grounds repo](https://gitlab.com/Meptl/cad/-/tree/main/gooselight).

Not really interesting, but I think it's too useful to not mention.

The flourescent starter in your kitchen is broken so you refuse to replace the
last bulb? Gooselight.

Need diffuse lighting for a good photo?
Double gooselight with paper taped around the LEDs.
Not this photo; I lacked a gooselight.

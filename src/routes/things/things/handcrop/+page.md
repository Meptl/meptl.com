---
title: Handcrop
date: July 24, 2024
dateunix: 1721878705
---

<script>
  import vid from "./example-run.mp4"
  import VideoPlayer from '$lib/VideoPlayer.svelte';
</script>

<VideoPlayer src={vid} />

For ages, I was looking for a simple video clipper that didn't involve
manually typing numbers for my window size and x-y offset.
You know, the numbers that if I already knew I could just run `ffmpeg` with?

Whilst complaining to my friend, he responds with
"Why don't you just make it? Seems simple enough."

"Bitch please" I thought.
This man works in software mind you.
Maybe he was under some influence that day.

Anyway after much too much time for what it's worth, I present:
[Handcrop](https://gitlab.com/Meptl/handcrop).
A GUI application that does two things: Crop resolution and trim duration of
video.

Technically I created a flatpak for this so it can be distributed to any Linux
distro, but hell if I'm going to make my mark on this world by submitting a PR
to Flathub.

<small>
All software is a frontend for ffmpeg.
All cloud hosts should instead run <a href="https://github.com/StreamPot/streampot">StreamPot</a> as a service.
</small>

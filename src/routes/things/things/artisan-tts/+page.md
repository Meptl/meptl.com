---
title: artisan-tts
date: Dec 05, 2023
dateunix: 1701803050
shitpost: true
---

<!-- Workaround from https://github.com/pngwn/MDsveX/issues/287 -->
<script context="module">
    import audio from "./hanzo.ogg"

    metadata.audio = audio
</script>

One-day build.
[Source code](https://gitlab.com/Meptl/artisan-tts).

Allows you to make janky TTS using a set of samples.
This is _hand-crafted_ TTS -- _artisanal_!

It's a script that expects a folder of audio files and text files and allows the
user to select audio files using the text file contents (which are presumably
transcriptions).
Then, it uses
[OpenAI's whisper](https://github.com/openai/whisper)
to transcript the audio.
The user can then select a segment from _that_ transcription.
Finally, it joins all selected audio segments into a single file.

---
title: ShareYourTube
date: August 25, 2024
dateunix: 1724607788
shitpost: true
---

<script>
    import image from "./preview.png"
    metadata.image = image
</script>

[share-your-tube.meptl.com](https://share-your-tube.meptl.com)

Create a shareable link to a mock Youtube front page similar to your own recommendations.
May take a bit to load first time because the free hosting I use is on-demand.
Also, there's a high probability that this no longer works since it parses Youtube's
HTML
but 🤷

The algorithm is truly wacky.
[And keeps giving me anime girl music...](https://share-your-tube.meptl.com/MQhDUIlGPeHiwG_JxV-vF)

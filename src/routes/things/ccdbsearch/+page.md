---
title: ccdbsearch
date: Aug 03, 2022
dateunix: 1659558947
shitpost: true
---

<!-- Workaround from https://github.com/pngwn/MDsveX/issues/287 -->
<script context="module">
    import image from "./ccdbsearch.webp"
    metadata.image = image
</script>

[ccdbsearch.meptl.com](https://ccdbsearch.meptl.com)
See if you're credit card information has been part of a security breach!

Svelte.

Additionally check out
[haveibeenpwned.com](https://haveibeenpwned.com)
and [pentester.com](https://pentester.com)

import ClickParticle from './ClickParticle.svelte';

export function spawnParticle(event, color, text) {
  const seed = Math.random();
  const duration = 1000 + seed * 1000 - 500;
  const particle = new ClickParticle({
    target: document.body,
    // $destroy is instant, so we animate via the intro.
    intro: true,
    props: {
      x: event.clientX,
      y: event.clientY,
      seed: seed,
      duration: duration,
      color: color,
      text: text
    }
  });
  // I was finding the animation to occasionally flicker back to the starting
  // location, so delete the particle before the animation duration finishes.
  setTimeout(function () {
    particle.$destroy();
  }, duration - 50);
}

export function notImplemented(event, color) {
  spawnParticle(event, color, 'No');
}

import { fetchSortedTwitsLiked } from '$lib/twits.js';
import { error } from '@sveltejs/kit';

import { readdirSync, readFileSync } from 'fs';
import { join, extname } from 'path';
import matter from 'gray-matter';

/** @type {import('./$types').PageServerLoad} */
export async function load({ params }) {
  const allPosts = await fetchSortedTwitsLiked();
  const post = allPosts.find((p) => p.meta.dateunix == params.id);
  if (!post) {
    return error(404, 'Not found');
  }

  const postsDirectory = join(process.cwd(), 'src/routes/twits/posts');
  const files = readdirSync(postsDirectory);
  let postMarkdown;
  for (const file of files) {
    if (extname(file) !== '.md') {
      continue;
    }
    const filePath = join(postsDirectory, file);
    const { data, content } = matter(readFileSync(filePath, 'utf-8'));

    if (data.dateunix == post.meta.dateunix) {
      postMarkdown = content.trim();
      break;
    }
  }

  return {
    post: post,
    markdown: postMarkdown
  };
}

export async function entries() {
  const allPosts = await fetchSortedTwitsLiked();
  return allPosts.map((post) => {
    return {
      id: String(post.meta.dateunix)
    };
  });
}

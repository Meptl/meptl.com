---
date: Aug 28, 2024
dateunix: 1724869338
---

<script>
    import DarkModeImage from '$lib/DarkModeImage.svelte';
    import im from './guyswould.png';
</script>

<DarkModeImage src={im} />

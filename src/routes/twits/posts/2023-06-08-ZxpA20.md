---
date: Jun 08, 2023
dateunix: 1686246732
---

The only difference between you and a pro CS:GO player is a high refresh rate
monitor and a ballin' GPU.

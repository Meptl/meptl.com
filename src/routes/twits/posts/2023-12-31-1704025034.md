---
date: Dec 31, 2023
dateunix: 1704025034
---

I did it! I did it!
I got a dream where I lost some of my teeth!

And all it took was a persistent fear of long term consequences of keeping my
achy wisdom teeth.

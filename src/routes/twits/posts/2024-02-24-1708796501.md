---
date: Feb 24, 2024
dateunix: 1708796501
---

### Blender Noise Animation Modifier on 3s

1. Have your F-curve animation modifier
1. Select two keyframes to specify a range
1. Bake your animation
   ![](./bake.webp)
1. Select new keyframes, move timeline to first frame, scale by 3.
   ![](./scalekeyframes.mp4)
1. Disable F-curve modifiers (wrench in Graph Editor) or remove modifiers
1. Set interpolation mode to "Constant"
   ![](./interpolation.webp)

Example - 3s at 24fps:
![](./microphone.mp4)

import { fetchSortedTwitsLiked } from '$lib/twits.js';

/** @type {import('./$types').PageServerLoad} */
export async function load() {
  let allPosts = await fetchSortedTwitsLiked();
  return {
    posts: allPosts
  };
}

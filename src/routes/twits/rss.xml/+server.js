import { fetchSortedTwits } from '$lib/twits.js';

const siteURL = 'https://meptl.com';
const siteTitle = 'Meptl twits';
const siteDescription = 'Meptl twit feed';

export const GET = async () => {
  const allPosts = await fetchSortedTwits();

  const body = render(allPosts);
  const options = {
    headers: {
      'Cache-Control': 'max-age=0, s-maxage=3600',
      'Content-Type': 'application/xml'
    }
  };

  return new Response(body, options);
};

const render = (posts) => `<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
<title>${siteTitle}</title>
<description>${siteDescription}</description>
<link>${siteURL}</link>
<atom:link href="${siteURL}/twits/rss.xml" rel="self" type="application/rss+xml"/>
${posts
  .map(
    (post) => `<item>
<guid isPermaLink="true">${siteURL}/twits/${post.meta.date}</guid>
<title>Twit</title>
<link>${siteURL}/twits</link>
<description><![CDATA[${post.content}]]></description>
<pubDate>${new Date(post.meta.date).toUTCString()}</pubDate>
</item>`
  )
  .join('')}
</channel>
</rss>
`;

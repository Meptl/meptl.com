<script>
  import { onMount } from 'svelte';

  onMount(() => {
    setInterval(() => {
        print();
    }, 50)
  });
</script>

This page calls print() every 50ms:

```
setInterval(() => {
    print();
}, 50)
```

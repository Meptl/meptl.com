import bpy
import librosa
import random

y, sr = librosa.load(bpy.context.scene.sound_nodes.audio_source)
duration = len(y) / sr
frames = int(duration * bpy.context.scene.render.fps)
bpy.context.scene.frame_end = frames
bpy.context.sequences[0].frame_final_duration = frames

bpy.data.materials["Gradient"].node_tree.nodes["Seed"].outputs[0].default_value = random.randint(0, 80000)

bpy.ops.sound_nodes.run_analysis()

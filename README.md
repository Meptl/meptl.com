# Meptl.com

## Deployment

This is deployed on Netlify.

Some environment variables are set:
- DATABASE_URL
- PRISMA_BINARY_TARGET=debian-openssl-1.1.x
- GIT_LFS_ENABLE=true
- GIT_LFS_FETCH_INCLUDE=\*.jpg,\*.png,\*.jpeg,\*.svg,\*.gif,\*.pdf,\*.mp4,\*.bmp,\*.webp,\*.mp3
- RUNPOD_API_KEY


## Development

### Setup

It helps to add this snippet to .git/hooks/pre-push

```
npm run build
if [ $? -ne 0 ]; then
  echo "Build failed. Aborting push."
  exit 1
fi
```
### Notes
Check out streaming content: https://svelte.dev/blog/streaming-snapshots-sveltekit

### Database

#### Local

- Run a local postgresql server for testing any db data stuff:

```
docker run --name localpostgres -e POSTGRES_PASSWORD=amongus -p 5432:5432 postgres
```

- Change DATABASE_URL in .envrc if you didn't use the above command
- Change the "provider" field in prisma/schema.prisma to "postgresql"
- Setup the schema if needed: `npx prisma db push`

To examine the database, you can use `npx prisma studio`

#### Updating Schema

- Update DATABASE_URL in .envrc
- Change the "provider" field in prisma/schema.prisma to "cockroachdb"
- For simple changes: `npx prisma db push`
- For more complex changes, read about the migration process.

### Idioms

#### Centering

Use flex

```
<div class="flex items-center justify-center"/>
```

#### Overlaying

Use grid with child elements sharing the same grid location.

```
<div class="grid w-32 h-32">
  <div class="col-start-1 row-start-1"/>
  <div class="col-start-1 row-start-1"/>
</div>
```

## Notes
A bit of CSS that will likely simplify a lot fo things I've done.
```
grid-template-areas: "stack";
grid-area: stack;
```

### Netlify
I use to host on Netlify, but they didn't let be easily retrieve client IP
addresses. Here's the netlify specific env vars that are required for the
site to work:
```
GIT_LFS_ENABLED=true
GIT_LFS_FETCH_INCLUDE=*.jpg,*.png,*.jpeg,*.svg,*.gif,*.pdf,*.mp4,*.bmp,*.webp,*.webm,*.mp3,*.ogg,*.stl
```

### Audio Normalization
```
ffmpeg-normalize output.wav -t '-14' -tp '-1' -o output2.wav
```

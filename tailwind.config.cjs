/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{html,js,svelte,ts,md}'],
  darkMode: 'class',
  theme: {
    extend: {
      animation: {
        'spin-slow': 'spin 12s linear infinite'
      },
      colors: {
        logo: '#ffbb3e',
        'site-white': '#f4f4f5', // Same as zinc-100
        'site-black': '#27272a' // Same as zinc-800
      },
      backgroundImage: {
        'dark-image': 'url("/pub/bg-tile-dark.webp")',
        'light-image': 'url("/pub/bg-tile-light.webp")'
      },

      // Disable ` in inline code blocks.
      typography: {
        DEFAULT: {
          css: {
            'code::before': {
              content: '""'
            },
            'code::after': {
              content: '""'
            }
          }
        }
      }
    }
  },
  plugins: [require('@tailwindcss/typography')]
};

#!/bin/sh

set -eou

if [ $# -ne 1 ]; then
    echo "Usage: post-voice-memo.sh <filename>"
    exit 1
fi

# If input.mp3 exists abort
if [ -f "input.mp3" ]; then
    echo "input.mp3 already exists. Aborting."
    exit 1
fi

# If output.mp4 exists abort
if [ -f "output.mp4" ]; then
    echo "output.mp4 already exists. Aborting."
    exit 1
fi

# If pre_final_output.mp4 exists abort
if [ -f "pre_final_output.mp4" ]; then
    echo "pre_final_output.mp4 already exists. Aborting."
    exit 1
fi

# If final_output.mp4 exists abort
if [ -f "final_output.mp4" ]; then
    echo "final_output.mp4 already exists. Aborting."
    exit 1
fi

fbname=${1%.*}

ffmpeg -i $1 input.mp3

blender -b ./twit-audio-memo/spectrogram.blend -P ./twit-audio-memo/pre-render.py -a

# Couldn't get audio output to work on blender export so attach it.
ffmpeg -i output.mp4 -i input.mp3 -c copy -map 0:v:0 -map 1:a:0 pre_final_output.mp4

# For some reason merging audios breaks playback on mobile. Just re-encode
ffmpeg -i pre_final_output.mp4 -map 0:v -c:v copy -b:v 1000k -map 0:a -c:a aac -b:a 128k final_output.mp4


echo "![no-autoplay no-loop max-300px](./$fbname.mp4)" > /tmp/post.md
./post.sh /tmp/post.md
mv final_output.mp4 src/routes/twits/posts/$fbname.mp4
git add src/routes/twits/posts/$fbname.mp4

rm output.mp4 input.mp3 pre_final_output.mp4

import adapter from '@sveltejs/adapter-auto';
import sveltePreprocess from 'svelte-preprocess';
import { mdsvex } from 'mdsvex';
import relativeImages from 'mdsvex-relative-images';
import rehypeSlug from 'rehype-slug';
import rehypeAutolinkHeadings from 'rehype-autolink-headings';
import rehypeExternalLinks from 'rehype-external-links';
import customMdsVexPlugin from './src/custom-mdsvex-plugin/index.js';

/** @type {import('@sveltejs/kit').Config} */
const config = {
  onwarn: (warning, handler) => {
    if (warning.code === 'a11y-media-has-caption') {
      return;
    }
    if (warning.code === 'a11y-missing-attribute') {
      return;
    }
    handler(warning);
  },
  kit: {
    adapter: adapter()
  },
  extensions: ['.svelte', '.md'],
  preprocess: [
    mdsvex({
      extensions: ['.md'],
      layout: {
        blog: './src/routes/blog/post_layout.svelte',
        things: './src/routes/things/things_layout.svelte',
        twits: './src/routes/twits/twits_layout.svelte'
      },
      remarkPlugins: [relativeImages, customMdsVexPlugin],
      rehypePlugins: [
        rehypeSlug,
        rehypeAutolinkHeadings,
        [
          rehypeExternalLinks,
          {
            target: '_blank',
            rel: ['noopener', 'noreferrer']
          }
        ]
      ]
    }),
    sveltePreprocess({
      postcss: true
    })
  ]
};

export default config;
